Term Blocks allows you to create blocks which list terms from specified vocabularies.
You can define the number of terms you want listed, as well as the order, such as most
recently used, most popular, random, or in a tag cloud.
